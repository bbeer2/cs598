# My project's README
CS598 project by Brian Beer
Instructions below are the required packages needed to run the News Aggy project
ALL PACKAGES should be installed and run using python3, several of these are NOT compatible with python 2.x

--Install the following packages, generally using 
LDA essentials
Install LDA package
https://pypi.python.org/pypi/lda

Install nltk
http://www.nltk.org/install.html

Install feedparser
https://pypi.python.org/pypi/feedparser

Install stopwords
https://pypi.python.org/pypi/stop-words

Web Framework
Install Flask
http://flask.pocoo.org/docs/0.11/installation/

--Download english lang  (all) package for tokenizer
Enter python3 interpreter and type
import nltk
nltk.download()



--Run instructions
Update model
python3 update_model.py

Update news
python update_news.py

Running app front-end
#export FLASK_APP=web_app.py 
#python3 -m flask run --host=0.0.0.0

--Additional notes
app.db stores topics model top words, location of images, and ranked news to be displayed. If this database it deleted, it will be re-generated automatically. Consider doing this on a clean install.
