import feedparser
from nltk.tokenize import RegexpTokenizer
import nltk
import string
from stop_words import get_stop_words
import os

tokenizer = RegexpTokenizer(r'\w+')
transtable = {ord(c): None for c in string.punctuation}
# create English stop words list
en_stop = get_stop_words('en')
path_to_images = 'static/images/'

rss_url = ["http://rss.cnn.com/rss/cnn_topstories.rss",
           "http://feeds.nytimes.com/nyt/rss/HomePage",
           "http://hosted.ap.org/lineups/USHEADS-rss_2.0.xml?SITE=RANDOM&SECTION=HOME",
           "http://rssfeeds.usatoday.com/usatoday-NewsTopStories",
           "http://www.npr.org/rss/rss.php?id=1001",
           "http://feeds.reuters.com/reuters/topNews",
           "http://newsrss.bbc.co.uk/rss/newsonline_world_edition/americas/rss.xml",
           "http://feeds.reuters.com/reuters/topNews",
           'http://feeds.abovetopsecret.com/1.xml',
           'http://feeds.feedburner.com/activistpost',
           'http://www.allgov.com/rss/news/',
           'http://feeds.feedblitz.com/alternet',
           'http://www.infowars.com/feed/custom_feed_rss',
           'http://feeds.feedburner.com/AMERICAblog',
           'http://www.thebureauinvestigates.com/feed/atom/',
           'http://www.blacklistednews.com/rss.php',
           'http://feeds.cato.org/CatoHomepageHeadlines',
           'http://www.commondreams.org/rss.xml',
           'http://feeds.feedburner.com/Consortiumnewscom',
           'http://feeds.feedburner.com/CorbettReportRSS',
           'http://www.counterpunch.org/category/article/feed/',
           'http://www.courthousenews.com/cns_feed.xml',
           'http://feeds.crooksandliars.com/crooksandliars/YaCP',
           'http://dailybail.com/home/rss.xml',
           'http://www.wired.com/category/dangerroom/feed/',
           'http://feeds.feedburner.com/democracynow/hVoT',
           'http://feeds.feedburner.com/disinfo/oMPh',
           'http://feeds.feedburner.com/economicpolicyjournal/YZSb',
           'https://www.eff.org/rss/updates.xml',
           'http://eyesopenreport.com/feed/',
           'http://fair.org/feed/',
           'http://www.freepress.net/blog/rss.xml',
           'http://www.globalpost.com/feed/dispatch/18797',
           'http://globalresearch.ca/feed',
           'http://thehill.com/taxonomy/term/27/feed',
           'http://inthesetimes.com/rss',
           'http://www.indexoncensorship.org/feed/',
           'http://www.accuracy.org/feed/?post_type=news-release',
           'http://www.ipsnews.net/feed/',
           'http://www.lewrockwell.com/feed/',
           'http://feeds.feedburner.com/LongWarJournalSiteWide',
           'http://www.mediachannel.org/feed/',
           'http://www.opednews.com/newsfeed.xml',
           'http://rt.com/rss/usa/',
           'http://talkingpointsmemo.com/feed/all',
           'http://thebulletin.org/search-feed',
           'http://www.thesleuthjournal.com/feed/',
           'http://thinkprogress.org/feed/',
           'http://truth-out.org/feed?format=feed',
           'http://feeds.feedburner.com/UsaWatchdog',
           'http://www.vice.com/rss',
           'http://www.voanews.com/api/',
           'http://www.washingtonsblog.com/feed',
           'http://feeds.feedburner.com/wrc',
           'http://whatreallyhappened.com/rss.xml',
           'http://whowhatwhy.com/feed/',
           'http://en.wikinews.org/w/index.php?title=Special:NewsFeed&feed=atom&categories=Published&notcategories=No%20publish%7CArchived%7CAutoArchived%7Cdisputed&namespace=0&count=30&hourcount=124&ordermethod=categoryadd&stablepages=only',
           'http://www.wnd.com/feed/',
           'http://feeds.feedburner.com/yes/most-recent-articles',
           ]

class document():
    count = 0

    def __init__(self, description, url, image=None):
        self.description = description
        self.url = url
        if image and os.path.exists(os.path.join(path_to_images, image)):
            self.image = os.path.join('images/', image)
        else:
            self.image = 'images/default.png'

class topic():
    def __init__(self, id, description, top_words, documents):
        self.id = id
        self.description = description
        self.top_words = top_words
        self.documents = documents

class topic_archive():
    def __init__(self, dates, topics):
        self.date_model_pair = []
        for index, date in enumerate(dates):
            self.date_model_pair.append((date, topics[index]))

def get_feeds(rss_url):
    total_count = 0
    doc_list = []
    for url in rss_url:
        count = 0
        feed = feedparser.parse(url)
        for item in feed['items']:
            count += 1
            tokens = nltk.word_tokenize(item['title'].translate(transtable))
            if len(tokens):
                doc_list.append((' '.join(tokens) + '\n', item['link']))
        total_count += count
    return doc_list

def write_feeds_to_file(rss_url, filename):
    with open(filename, 'w') as file:
        total_count = 0
        for url in rss_url:
            count = 0
            feed = feedparser.parse(url)
            for item in feed['items']:
                count += 1
                tokens = nltk.word_tokenize(item['title'].translate(transtable))
                if len(tokens):
                    file.write(' '.join(tokens) + '\n')
            total_count += count
            file.flush()
            #print(count)
        #print(total_count)

def get_topic(document):
    doc_tokens = document.split()
    stopped_tokens = [i for i in doc_tokens if not i in en_stop]

    doc_topics = ldamodel[dictionary.doc2bow(stopped_tokens)]
    print(doc_topics)
    return max(doc_topics, key=lambda item: item[1])[0]

def sorted_by_topic(doc_topics_prob, topic_index):
    return sorted(doc_topics_prob, key=lambda x: x[0][topic_index][1], reverse=True)