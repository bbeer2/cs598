import feedparser

rss_url = ["http://rss.cnn.com/rss/cnn_topstories.rss",
           "http://feeds.nytimes.com/nyt/rss/HomePage",
           "http://hosted.ap.org/lineups/USHEADS-rss_2.0.xml?SITE=RANDOM&SECTION=HOME",
           "http://rssfeeds.usatoday.com/usatoday-NewsTopStories",
           "http://www.npr.org/rss/rss.php?id=1001",
           "http://feeds.reuters.com/reuters/topNews",
           "http://newsrss.bbc.co.uk/rss/newsonline_world_edition/americas/rss.xml",
           "http://feeds.reuters.com/reuters/topNews",
           'http://feeds.abovetopsecret.com/1.xml',
           'http://feeds.feedburner.com/activistpost',
           'http://www.allgov.com/rss/news/',
           'http://feeds.feedblitz.com/alternet',
           'http://www.infowars.com/feed/custom_feed_rss',
           'http://feeds.feedburner.com/AMERICAblog',
           'http://www.thebureauinvestigates.com/feed/atom/',
           'http://www.blacklistednews.com/rss.php',
           'http://feeds.cato.org/CatoHomepageHeadlines',
           'http://www.commondreams.org/rss.xml',
           'http://feeds.feedburner.com/Consortiumnewscom',
           'http://feeds.feedburner.com/CorbettReportRSS',
           'http://www.counterpunch.org/category/article/feed/',
           'http://www.courthousenews.com/cns_feed.xml',
           'http://feeds.crooksandliars.com/crooksandliars/YaCP',
           'http://dailybail.com/home/rss.xml',
           'http://www.wired.com/category/dangerroom/feed/',
           'http://feeds.feedburner.com/democracynow/hVoT',
           'http://feeds.feedburner.com/disinfo/oMPh',
           'http://feeds.feedburner.com/economicpolicyjournal/YZSb',
           'https://www.eff.org/rss/updates.xml',
           'http://eyesopenreport.com/feed/',
           'http://fair.org/feed/',
           'http://www.freepress.net/blog/rss.xml',
           'http://www.globalpost.com/feed/dispatch/18797',
           'http://globalresearch.ca/feed',
           'http://thehill.com/taxonomy/term/27/feed',
           'http://inthesetimes.com/rss',
           'http://www.indexoncensorship.org/feed/',
           'http://www.accuracy.org/feed/?post_type=news-release',
           'http://www.ipsnews.net/feed/',
           'http://www.lewrockwell.com/feed/',
           'http://feeds.feedburner.com/LongWarJournalSiteWide',
           'http://www.mediachannel.org/feed/',
           'http://www.opednews.com/newsfeed.xml',
           'http://rt.com/rss/usa/',
           'http://talkingpointsmemo.com/feed/all',
           'http://thebulletin.org/search-feed',
           'http://www.thesleuthjournal.com/feed/',
           'http://thinkprogress.org/feed/',
           'http://truth-out.org/feed?format=feed',
           'http://feeds.feedburner.com/UsaWatchdog',
           'http://www.vice.com/rss',
           'http://www.voanews.com/api/',
           'http://www.washingtonsblog.com/feed',
           'http://feeds.feedburner.com/wrc',
           'http://whatreallyhappened.com/rss.xml',
           'http://whowhatwhy.com/feed/',
           'http://en.wikinews.org/w/index.php?title=Special:NewsFeed&feed=atom&categories=Published&notcategories=No%20publish%7CArchived%7CAutoArchived%7Cdisputed&namespace=0&count=30&hourcount=124&ordermethod=categoryadd&stablepages=only',
           'http://www.wnd.com/feed/',
           'http://feeds.feedburner.com/yes/most-recent-articles',
           ]

total_count = 0
for url in rss_url:
    feed = feedparser.parse(url)
    count = 0
    for item in feed['items']:
        count += 1
        #print(item['title'])
    total_count += count
    #print('Subtotal: {} - {}'.format(total_count, url))
    if not count:
        print('Link {} FAILED'.format(url))
    if count > 50:
        print(url)
print('Final count: {}'.format(total_count))




