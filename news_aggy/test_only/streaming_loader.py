import gensim

import newsaggy_common

rss_url = ["http://rss.cnn.com/rss/cnn_topstories.rss",
           ]



ldamodel = gensim.models.ldamodel.LdaModel.load('test_model.mm', mmap='r')
dictionary = ldamodel.id2word
topics = ldamodel.print_topics(num_words=10)
for topic in topics:
    print(topic)

docs = remote.cs598.news_aggy.newsaggy_common.get_feeds(rss_url)
doc_topics_prob = []
for doc, doc_url in docs:
    doc_tokens = doc.split()
    doc_topics_prob.append((ldamodel.get_document_topics(dictionary.doc2bow(doc_tokens)), doc, doc_url))
for k in range(5):
    sorted_topic = remote.cs598.news_aggy.newsaggy_common.sorted_by_topic(doc_topics_prob, k)
    print('Topic {}:'.format(k))
    for i in range(10):
        _, doc, doc_url = sorted_topic[i]
        print(doc)