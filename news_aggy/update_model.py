import gensim
from gensim import corpora
from stop_words import get_stop_words
import newsaggy_common
import sqlite3
import datetime

class MyCorpus(object):
    def __iter__(self):
        for line in open('news.txt'):
        # assume there's one document per line, tokens separated by whitespace
            doc_tokens = line.lower().split()
            # remove stop words from tokens
            stopped_tokens = [i for i in doc_tokens if not i in en_stop]
            yield dictionary.doc2bow(stopped_tokens)

def create_tables():
    cursor.execute('''create table if not exists archive(date DATE, id INTEGER, word TEXT, probability FLOAT, rank INTEGER)''')
    conn.commit()

def get_topic(document):
    doc_tokens = document.split()
    stopped_tokens = [i for i in doc_tokens if not i in en_stop]


    doc_topics = ldamodel[dictionary.doc2bow(stopped_tokens)]
    print(doc_topics)
    return max(doc_topics, key=lambda item: item[1])[0]

def store_topic_archive(model, k_topics, n_words_to_store):
    today = datetime.datetime.now()
    for k in range(k_topics):
        top_words = model.show_topic(k, topn=n_words_to_store)
        rank = 0
        for word, probability in top_words:
            cursor.execute('''INSERT INTO archive(date, id, word, probability, rank) VALUES(?,?,?,?,?)''', (today, k, word, probability, rank))
            rank += 1
    conn.commit()

conn = sqlite3.connect('app.db')
cursor = conn.cursor()
create_tables()

# create English stop words list
en_stop = get_stop_words('en')

docs = newsaggy_common.write_feeds_to_file(newsaggy_common.rss_url, 'news.txt')
dictionary = corpora.Dictionary(line.lower().split() for line in open('news.txt'))
corpus_memory_friendly = MyCorpus()


ldamodel = gensim.models.ldamodel.LdaModel(corpus_memory_friendly, num_topics=5, id2word=dictionary, passes=100)
# Save model
ldamodel.save('test_model.mm')
topics = ldamodel.print_topics(num_words=10)
for topic in topics:
    print(topic)

#Store topic models to archive
store_topic_archive(ldamodel, 5, 10)


cursor.close()
