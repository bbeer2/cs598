import os
import sqlite3
import uuid

import gensim
import requests

import newsaggy_common


def create_tables():
    cursor.execute('''create table if not exists images(url TEXT PRIMARY KEY, filename TEXT)''')
    cursor.execute("drop table if exists ranked")
    cursor.execute('''create table if not exists ranked(url TEXT, title TEXT, topic INTEGER, rank INTEGER)''')
    cursor.execute("drop table if exists topics")
    cursor.execute('''create table if not exists topics(id INTEGER, word TEXT, probability FLOAT)''')
    conn.commit()

def download_images(doc_url, path_to_images):
    filename = '{}.jpg'.format(uuid.uuid4())
    data = ' '.join(['\"url\": \"{}\",'.format(doc_url),
                     '\"format\": \"jpg\"', ])
    print('curl -X POST -d {} {}'.format(data, 'https://api.url2img.com'))
    results = requests.get("https://api.url2img.com",
                           params={'url': doc_url})
    if results.status_code == requests.codes.ok:
        with open(os.path.join(path_to_images, filename), 'wb') as file:
            file.write(results.content)
        cursor.execute("SELECT * FROM images WHERE url = ?", (doc_url,))
        data = cursor.fetchall()
        if len(data) == 0:
            cursor.execute('''INSERT INTO images(url, filename)
                                      VALUES(?,?)''', (doc_url, filename))
    conn.commit()

def rank_and_store_top_docs(k_topics, n_docs_to_store):
    images_to_dl = []
    for k in range(k_topics):
        sorted_topic = newsaggy_common.sorted_by_topic(doc_topics_prob, k)
        print('Topic {}:'.format(k))
        for rank in range(n_docs_to_store):
            _, doc, doc_url = sorted_topic[rank]
            print(doc)
            cursor.execute('''INSERT INTO ranked(url, title, topic, rank) VALUES(?,?,?,?)''', (doc_url, doc, k, rank))
            images_to_dl.append(doc_url)
    conn.commit()
    return images_to_dl

def store_topic_top_words(model, k_topics, n_words_to_store):
    for k in range(k_topics):
        top_words = model.show_topic(k, topn=n_words_to_store)
        for word, probability in top_words:
            print(word)
            print(probability)
            cursor.execute('''INSERT INTO topics(id, word, probability) VALUES(?,?,?)''', (k, word, probability))
    conn.commit()

conn = sqlite3.connect('app.db')
cursor = conn.cursor()
create_tables()

rss_url = ["http://rss.cnn.com/rss/cnn_topstories.rss",]
           #"http://feeds.nytimes.com/nyt/rss/HomePage",
           #"http://www.washingtonpost.com/rss/"]
path_to_images = 'static/images/'

ldamodel = gensim.models.ldamodel.LdaModel.load('test_model.mm', mmap='r')
dictionary = ldamodel.id2word

docs = newsaggy_common.get_feeds(newsaggy_common.rss_url)

#Update feeds
total_count = 0
doc_topics_prob = []
for doc, doc_url in docs:
    total_count += 1
    doc_tokens = doc.split()
    doc_topics_prob.append((ldamodel.get_document_topics(dictionary.doc2bow(doc_tokens), minimum_probability=0), doc, doc_url))

#Rank and update db entries
images_to_dl = rank_and_store_top_docs(5, 10)
store_topic_top_words(ldamodel, 5, 10)

#Download images
download_threads = []
total_count = 0
for doc_url in images_to_dl:
    total_count += 1
    download_images(doc_url, path_to_images)

cursor.close()