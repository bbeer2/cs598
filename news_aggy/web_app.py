from flask import Flask, render_template, request, send_from_directory
import itertools
from newsaggy_common import document, topic, topic_archive
import sqlite3

app = Flask(__name__, static_url_path='')
conn = sqlite3.connect('app.db')
cursor = conn.cursor()

def retrieve_ranked_docs(topic_k, top_n_docs):
    ranked_docs = []
    for rank in range(top_n_docs):
        cursor.execute("SELECT url, title FROM ranked WHERE topic = ? and rank = ?", (topic_k, rank))
        url, title = cursor.fetchone()
        cursor.execute("SELECT filename FROM images WHERE url = ?", (url,))
        result = cursor.fetchone()
        if result:
            ranked_docs.append(document(title, url, image=result[0]))
        else:
            ranked_docs.append(document(title, url))
    return ranked_docs

def retrieve_top_words(topic_k):
    cursor.execute("SELECT word, probability FROM topics WHERE id = ? ORDER BY probability DESC", (topic_k,))
    word_prob = cursor.fetchall()
    return word_prob

def get_topic_archive(k_topics):
    cursor.execute("SELECT distinct date FROM archive ORDER BY date DESC")
    dates = cursor.fetchall()
    list_of_dates = []
    word_prob_list = []
    for date in dates:
        single_date_word_prob_list = []
        list_of_dates.append(date[0])
        for k in range(k_topics):
            cursor.execute("SELECT word FROM archive WHERE date = ? and id = ? ORDER BY probability DESC", (date[0], k))
            single_date_word_prob_list.append(cursor.fetchall())
        word_prob_list.append(single_date_word_prob_list)
    return topic_archive(list_of_dates, word_prob_list)

@app.route('/')
def main_page():
    t1_docs = retrieve_ranked_docs(0, 10)
    t2_docs = retrieve_ranked_docs(1, 10)
    t3_docs = retrieve_ranked_docs(2, 10)
    t4_docs = retrieve_ranked_docs(3, 10)
    t5_docs = retrieve_ranked_docs(4, 10)

    topic1 = topic(1, 'topic 1.', retrieve_top_words(0), t1_docs)
    topic2 = topic(2, 'topic 2.', retrieve_top_words(1), t2_docs)
    topic3 = topic(3, 'topic 3.', retrieve_top_words(2), t3_docs)
    topic4 = topic(4, 'topic 4.', retrieve_top_words(3), t4_docs)
    topic5 = topic(5, 'topic 5.', retrieve_top_words(4), t5_docs)
    items = itertools.zip_longest(t1_docs, t2_docs, t3_docs, t4_docs, t5_docs)
    topics = [topic1, topic2, topic3, topic4, topic5]
    return render_template('main.html', topics=topics, items=items)

@app.route('/topic/<tid>')
def topic_page(tid=None):
    t1_docs = retrieve_ranked_docs(0, 10)
    t2_docs = retrieve_ranked_docs(1, 10)
    t3_docs = retrieve_ranked_docs(2, 10)
    t4_docs = retrieve_ranked_docs(3, 10)
    t5_docs = retrieve_ranked_docs(4, 10)

    topic1 = topic(1, 'topic 1.', retrieve_top_words(0), t1_docs)
    topic2 = topic(2, 'topic 2.', retrieve_top_words(1), t2_docs)
    topic3 = topic(3, 'topic 3.', retrieve_top_words(2), t3_docs)
    topic4 = topic(4, 'topic 4.', retrieve_top_words(3), t4_docs)
    topic5 = topic(5, 'topic 5.', retrieve_top_words(4), t5_docs)
    topics = [topic1, topic2, topic3, topic4, topic5]
    return render_template('topic.html', topic=topics[int(tid)-1])

@app.route('/archive')
def topic_archive_page():
    topic_archive = get_topic_archive(5)
    return render_template('archive.html', topic_archive=topic_archive)

if __name__ == "__main__":
    app.run()
